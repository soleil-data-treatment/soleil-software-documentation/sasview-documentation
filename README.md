# Aide et ressources de SASVIEW pour Synchrotron SOLEIL

[<img src="https://www.sasview.org/img/sasview_logo.png" width="250"/>](https://www.sasview.org/documentation/)

## Résumé

- Ajustement des données de diffusion des rayons X aux petits angles (SAXS) avec les différents modèles de facteur de structure proposé.
- Open source

## Sources

- Code source: https://github.com/SasView/
- Documentation officielle: https://www.sasview.org/docs/index.html

## Navigation rapide


| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](https://www.sasview.org/download/) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/120/sasview) |
| [Tutoriaux officiels](https://www.sasview.org/documentation/) |  |
| [Chaine Youtube officielle](https://www.youtube.com/channel/UCxvD3ysXJ05l6MgY7YKjEFQ) ||

## Installation

- Systèmes d'exploitation supportés:
- Installation: gérer dans le package déployé par GRADES

## Format de données

- en entrée: donnée réduite par PyFAI,  fichier de type Intensité =(fQ)
- en sortie: tableau de donnée *.txt ou *.dat
- sur la Ruche
